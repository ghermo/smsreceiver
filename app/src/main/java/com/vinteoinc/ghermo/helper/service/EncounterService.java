package com.vinteoinc.ghermo.helper.service;

import com.vinteoinc.ghermo.model.Encounter;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by grg021 on 5/25/15.
 */
public interface EncounterService {

    @FormUrlEncoded
    @POST("/api/v1/encounters")
    Encounter postEncounter(
            @Field("data") String data
    );

}
