package com.vinteoinc.ghermo.helper.service;

import android.content.SharedPreferences;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by grg021 on 5/25/15.
 */
public class RetrofitSpiceService extends BaseRetrofitSpiceService {

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
//                SharedPreferences settings = getSharedPreferences(BaseHelper.getSharedPreference(), 0);
//                String access_token = settings.getString("access_token", null);
//                if (access_token != null) {
//                    request.addHeader("Authorization", access_token);
//                }
            }
        };
        return super.createRestAdapterBuilder().setRequestInterceptor(requestInterceptor).setLogLevel(RestAdapter.LogLevel.FULL);
    }

}
