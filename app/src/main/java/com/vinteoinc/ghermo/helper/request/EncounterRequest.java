package com.vinteoinc.ghermo.helper.request;

import com.vinteoinc.ghermo.helper.service.EncounterService;
import com.vinteoinc.ghermo.model.Encounter;

/**
 * Created by grg021 on 5/25/15.
 */
public class EncounterRequest extends BaseRetrofitSpiceRequest<Encounter, EncounterService> {

    private String data;

    public EncounterRequest(String data) {
        super(Encounter.class, EncounterService.class);
        this.data = data;
    }

    @Override
    public Encounter loadDataFromNetwork() throws Exception {
        return getService().postEncounter(data);
    }

    public String createCacheKey() {
        return "encounter";
    }

}
