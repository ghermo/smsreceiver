package com.vinteoinc.ghermo.helper.service;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

/**
 * Created by grg021 on 5/25/15.
 */
public abstract class BaseRetrofitSpiceService extends RetrofitGsonSpiceService {

    private final String url = "http://api.vinteoinc.com";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(EncounterService.class);
    }

    @Override
    protected String getServerUrl() {
        return this.url;
    }
}
