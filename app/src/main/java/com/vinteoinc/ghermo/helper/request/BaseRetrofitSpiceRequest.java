package com.vinteoinc.ghermo.helper.request;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.octo.android.robospice.retry.RetryPolicy;
import com.vinteoinc.ghermo.util.VinteoRetryPolicy;

/**
 * Created by grg021 on 5/25/15.
 */
public abstract class BaseRetrofitSpiceRequest<T, R> extends RetrofitSpiceRequest<T, R> {

    private RetryPolicy retryPolicy = new VinteoRetryPolicy();

    public BaseRetrofitSpiceRequest(Class<T> clazz, Class<R> retrofitedInterfaceClass) {
        super(clazz, retrofitedInterfaceClass);
        setRetryPolicy(retryPolicy);
    }

}
