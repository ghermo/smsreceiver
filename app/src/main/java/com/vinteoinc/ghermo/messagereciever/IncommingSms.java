package com.vinteoinc.ghermo.messagereciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.vinteoinc.ghermo.helper.request.EncounterRequest;
import com.vinteoinc.ghermo.helper.service.RetrofitSpiceService;
import com.vinteoinc.ghermo.model.Encounter;

/**
 * Created by grg021 on 5/24/15.
 */
public class IncommingSms extends BroadcastReceiver {

    final SmsManager sms = SmsManager.getDefault();
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;

        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

//                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);
                    performEncounter(context, message, DurationInMillis.ALWAYS_EXPIRED);


                    // Show Alert
//                    int duration = Toast.LENGTH_LONG;
//                    Toast toast = Toast.makeText(context,"senderNum: " + senderNum + ", message: " + message, duration);
//                    toast.show();

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
//            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }

    }

    SpiceManager spiceManager = new SpiceManager(RetrofitSpiceService.class);

    private void startSpiceManager(Context context) {
        if (!spiceManager.isStarted()) {
            spiceManager.start(context);
        }
    }

    private void stopSpiceManager() {
        if (spiceManager.isStarted() && spiceManager.getPendingRequestCount() == 0) {
            spiceManager.shouldStop();
        }
    }

    private void performEncounter(Context context, String data, long duration) {
        startSpiceManager(context);
        EncounterRequest encounterRequest = new EncounterRequest(data);
        spiceManager.execute(encounterRequest, encounterRequest.createCacheKey(), duration, new encounterListener());
    }

    private class encounterListener implements RequestListener<Encounter> {
        @Override
        public void onRequestFailure(SpiceException spiceException) {
            stopSpiceManager();
//            Toast.makeText(context, spiceException.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Encounter encounter) {
            stopSpiceManager();
//            Toast.makeText(context, encounter.getTimestamp(), Toast.LENGTH_SHORT).show();

        }
    }
}
